<?php

namespace Skyeng\DateTime;

/**
 * A time-based amount of time.
 * Duration is not connected to the timeline, therefore units like "months" and "years" make no sense for this concept.
 *
 * Unlike Java 8 Duration, this concept doesn't keep information about units,
 * i.e. Duration::hours(2) and Duration::minutes(120) are the same in any sense.
 */
final class Duration
{
    private $seconds;
    private $negated;

    public static function days(int $days): self
    {
        self::assertNonNegative($days, 'Negative number of days is not allowed, use ->negated() explicitly.');

        return new self($days * 86400, false);
    }

    public static function hours(int $hours): self
    {
        self::assertNonNegative($hours, 'Negative number of hours is not allowed, use ->negated() explicitly.');

        return new self($hours * 3600, false);
    }

    public static function minutes(int $minutes): self
    {
        self::assertNonNegative($minutes, 'Negative number of minutes is not allowed, use ->negated() explicitly.');

        return new self($minutes * 60, false);
    }

    public static function seconds(int $seconds): self
    {
        self::assertNonNegative($seconds, 'Negative number of seconds is not allowed, use ->negated() explicitly.');

        return new self($seconds, false);
    }

    /**
     * @param string $duration ISO-8601 format
     * @return Duration
     */
    public function fromString(string $duration): self
    {
        try {
            $interval = new \DateInterval($duration);
        } catch (\Exception $e) {
            throw new \LogicException(sprintf('Duration does not follow ISO-8601 format: "%s".', $duration), 0, $e);
        }

        if ($interval->y !== 0 || $interval->m !== 0) {
            throw new \LogicException(sprintf('Months and years make no sense for duration.'));
        }

        return new self(
            $interval->s + $interval->m * 60 + $interval->h * 3600 + $interval->d * 86400,
            $interval->invert === 1
        );
    }

    private function __construct(int $seconds, bool $negated)
    {
        $this->seconds = $seconds;
        $this->negated = $negated;
    }

    public function equals(self $duration): bool
    {
        return $this->seconds === $duration->seconds && $this->negated === $duration->negated;
    }

    public function toSeconds(): int
    {
        return $this->seconds;
    }

    public function toSignedSeconds(): int
    {
        return $this->negated ? -$this->seconds : $this->seconds;
    }

    public function isNegated(): bool
    {
        return $this->negated;
    }

    public function negated(): self
    {
        return new self($this->seconds, $this->negated ? false : true);
    }

    public function plusSeconds(int $seconds): self
    {
        self::assertNonNegative($seconds, 'year');
        return new self($sec = $this->toSignedSeconds() + $seconds, $sec < 0);
    }

    public function minusSeconds(int $seconds): self
    {
        self::assertNonNegative($seconds, 'year');
        return new self($sec = $this->toSignedSeconds() - $seconds, $sec < 0);
    }

    public function plusMinutes(int $minutes): self
    {
        self::assertNonNegative($minutes, 'Negative number of minutes is not allowed, use ->minusMinutes() explicitly.');
        return new self($sec = $this->toSignedSeconds() + $minutes * 60, $sec < 0);
    }

    public function minusMinutes(int $minutes): self
    {
        self::assertNonNegative($minutes, 'Negative number of minutes makes no sense.');
        return new self($sec = $this->toSignedSeconds() - $minutes * 60, $sec < 0);
    }

    public function plusHours(int $hours): self
    {
        self::assertNonNegative($hours, 'Negative number of hours is not allowed, use ->minusHours() explicitly.');
        return new self($sec = $this->toSignedSeconds() + $hours * 3600, $sec < 0);
    }

    public function minusHours(int $hours): self
    {
        self::assertNonNegative($hours, 'Negative number of hours makes no sense.');
        return new self($sec = $this->toSignedSeconds() + $hours * 3600, $sec < 0);
    }

    public function plusDays(int $days): self
    {
        self::assertNonNegative($days, 'day');
        return new self($sec = $this->toSignedSeconds() + $days * 3600, $sec < 0);
    }

    public function minusDays(int $days): self
    {
        self::assertNonNegative($days, 'day');
        return new self($sec = $this->toSignedSeconds() - $days * 3600, $sec < 0);
    }

    private static function assertNonNegative(int $value, string $unitName): void
    {
        if ($value < 0) {
            throw new \LogicException(
                sprintf(
                    'Negative number of %1$ss is not allowed, use one of the proper methods: plus%2$s(), minus%2$s()',
                    $unitName,
                    ucfirst($unitName)
                )
            );
        }
    }
}
