<?php

namespace Skyeng\DateTime;

use DateTimeImmutable;

/**
 *
 */
final class LocalDate
{
    private $date;

    public static function from(int $hour, int $minute, int $second): self
    {
        return new self($hour, $minute, $second);
    }

    public static function fromString(string $date): self
    {
        if (!preg_match('/^(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2})$/', $date, $match)) {
            throw new \LogicException(sprintf('Date must respect YYYY-MM-DD local date format.'));
        }

        return new self($match['year'], $match['month'], $match['day'] ?? 0);
    }

    private function __construct(int $year, int $month, int $day)
    {
        if (!checkdate($month, $day, $year)) {
            throw new \LogicException(sprintf('Not a date: %04d-%02d-%02d', $year, $month, $day));
        }

        $this->date = sprintf('%04d-%02d-%02d', $year, $month, $day);
    }

    public function equals(self $time): bool
    {
        return $this->date === $time->date;
    }

    public function isBefore(self $time): bool
    {
        return $this->date < $time->date;
    }

    public function isAfter(self $time): bool
    {
        return $this->date > $time->date;
    }

    public function toString(): string
    {
        return $this->date;
    }

    public function plusDays(int $days): self
    {
        self::assertNonNegative($days, 'day');
        return self::fromString((new DateTimeImmutable(sprintf('+%d days', $days)))->format('Y-m-d'));
    }

    public function minusDays(int $days): self
    {
        self::assertNonNegative($days, 'day');
        return self::fromString((new DateTimeImmutable(sprintf('-%d days', $days)))->format('Y-m-d'));
    }

    private static function assertNonNegative(int $value, string $unitName): void
    {
        if ($value < 0) {
            throw new \LogicException(
                sprintf(
                    'Negative number of %1$ss is not allowed, use one of the proper methods: plus%2$s(), minus%2$s()',
                    $unitName,
                    ucfirst($unitName)
                )
            );
        }
    }
}
