<?php

namespace Skyeng\DateTime;

use DateInterval;

/**
 * A date-based amount of time in the ISO-8601 calendar system (years, months, days).
 */
final class Period
{
    /**
     * @var DateInterval
     */
    private $interval;

    public static function between(LocalDate $startDateInclusive, LocalDate $endDateExclusive): self
    {
        $date1 = new \DateTimeImmutable($startDateInclusive->toString());
    }

    private function __construct(DateInterval $interval)
    {
        $this->interval = $interval;
    }
}
