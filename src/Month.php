<?php

namespace Skyeng\DateTime;

/**
 *
 */
final class Month
{
    private $month;

    public static function january(): self
    {
        return new self(1);
    }

    public static function february(): self
    {
        return new self(2);
    }

    public static function march(): self
    {
        return new self(3);
    }

    public static function april(): self
    {
        return new self(4);
    }

    public static function may(): self
    {
        return new self(5);
    }

    public static function june(): self
    {
        return new self(6);
    }

    public static function july(): self
    {
        return new self(7);
    }

    public static function august(): self
    {
        return new self(8);
    }

    public static function september(): self
    {
        return new self(9);
    }

    public static function october(): self
    {
        return new self(10);
    }

    public static function november(): self
    {
        return new self(11);
    }

    public static function december(): self
    {
        return new self(12);
    }

    public static function fromInt(int $month)
    {
        return new self($month);
    }

    private function __construct(int $month)
    {
        if ($month < 1 || $month > 12) {
            throw new \LogicException('Invalid month number: %d.', $month);
        }

        $this->month = $month;
    }

    public function equals(self $month): bool
    {
        return $this->month === $month->month;
    }
}
